import json, csv
import boto3
from botocore.exceptions import ClientError
from chalice import NotFoundError, Chalice
from io import StringIO

app = Chalice(app_name='mockapis')
S3 = boto3.client('s3', region_name='us-west-2')
BUCKET = 'rochester-gs'
KEY = "weather_data.json"
CSV_KEY = "weather_data.csv"

cache_data = None

@app.route('/objects', methods=['GET', 'PUT'])
def s3objects():
    global cache_data, KEY
    request = app.current_request
    
    if request.method == 'GET':
        
        if cache_data is None:
            
            try:
                response = S3.get_object(Bucket=BUCKET, Key=KEY)
                cache_data = json.loads(response['Body'].read())
            except Exception as e:
                import codecs
                cache_data = None
                data = S3.get_object(Bucket=BUCKET, Key=CSV_KEY)
                dict_data = {}

                for row in csv.DictReader(codecs.getreader("utf-8")(data["Body"])):
                    dict_data[row["datetime"]] = row

            
                S3.put_object(
                    Body=str(json.dumps(dict_data)),
                    Bucket=BUCKET,
                    Key=KEY
                )
                cache_data = json.loads(response['Body'].read())


        return {"data": cache_data[request.query_params["dateTime"]]}